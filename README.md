# Go-ing for certs

As I start down the journey for my eJPT and other security certifications, this will hold all the Go code that I create as I learn programming languages along side them.

## The Idea

I firmly believe that whether it's a programming language or a spoken/written language, the best way to learn it is by simply doing it. With the goal of learning both red team and blue team methods and getting certifications to demonstrate that knowledge, I want to also learn Go, Python, and Bash along with it. While I go through various certification courses, I'll be attempting to create my own tools to accomplish tasks. 

The code will be stored in this repo along with some general notes, but write-ups and more fleshed-out ideas will be posted on [my blog](https://pshef.work).

## The Resources

I'm using ["Automate the Boring Stuff" by Al Sweigart](https://automatetheboringstuff.com/) (both the book and the Udemy course) and the ["Python Crash Course" by Eric Matthes](https://nostarch.com/pythoncrashcourse2e) book to learn python, for Bash I'm using ["The Linux Command Line" by William Shotts](https://nostarch.com/tlcl2) and ["Linux Basics for Hackers" by OccupyTheWeb](https://nostarch.com/linuxbasicsforhackers), and for Go I'm using the Udemy courses ["A gentle introduction to Google Go (Golang) for Beginners" by David Valentine](https://www.udemy.com/course/a-gentle-introduction-to-google-go-golang-for-beginners/) and ["Learn How To Code: Google's Go (golang) Programming Language" by Todd McLeod](https://www.udemy.com/course/learn-how-to-code/). 

Additionally, I'll use the books ["Black Hat Go" by Tom Steele, Chris Patten, and Dan Kottmann"](https://nostarch.com/blackhatgo) and ["Black Hat Python 2nd Edition" by Justin Seitz and Tim Arnold"](https://nostarch.com/black-hat-python2E) as inspiration and guidance while I figure out what I'm actually doing.

## The Goal

For the languages, it's fairly simple: I want to be able to read and write my own scripts. For cybersecurity, there are two certifications *specifically* that I want to go for, and those are the eJPT and PNPT. While there are others that I want to get down the road and I'd use this repository to keep track of my progress, I don't want to get too far ahead of myself before life changes and I decide to go for something different.

